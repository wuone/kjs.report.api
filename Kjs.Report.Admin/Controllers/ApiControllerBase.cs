﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Kjs.Report.Admin.Controllers
{
    /// <summary>
    /// /// <summary>
    /// api控制器基类
    /// </summary>
    /// </summary>
    public class ApiControllerBase : Controller
    {
        /// <summary>
        /// 客户端IP
        /// </summary>  
        public string Ip
        {
            get
            {
                return HttpContext.Connection.RemoteIpAddress.ToString();
            }
        }

        /// <summary>
        /// 用户对象
        /// </summary>
        public new ClaimsPrincipal User
        {
            get
            {
                return HttpContext.User;
            }
        }
    }
}
