﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Kjs.Report.IService;
using Microsoft.AspNetCore.Authorization;

namespace Kjs.Report.Admin.Controllers
{
    /// <summary>
    /// 终端
    /// </summary>
    [Produces("application/json")]
    [Authorize("Bearer")]
    [EnableCors("Any")]
    [Route("api/terminal")]
    public class TerminalController : ApiControllerBase
    {
        private ITerminalService _terminalService;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="terminalService"></param>
        public TerminalController(ITerminalService terminalService)
        {
            _terminalService = terminalService;
        }


        /// <summary>
        /// 按照时间分组统计终端明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="province_id">省编号</param>
        /// <param name="city_id">市编号</param>
        /// <param name="area_id">区编号</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("timegroup/page/list")]
        public object TimeGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int province_id = 0, int city_id = 0, int area_id = 0,
             int line_id = 0, int point_id = 0, DateTime? beginTime = null, DateTime? endTime = null)
        {
            return _terminalService.TimeGroupList(pageSize, pageIndex, "", province_id, city_id, area_id, orgIds, line_id, point_id, beginTime, endTime);
        }
    }
}
