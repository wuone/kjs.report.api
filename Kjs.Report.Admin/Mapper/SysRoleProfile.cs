﻿using AutoMapper;
using Kjs.Report.Domain.Table;
using Kjs.Report.Model;

namespace Kjs.Report.Admin.Mapper
{
    /// <summary>
    /// 角色mapper类
    /// </summary>
    public class SysRoleProfile: Profile
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        public SysRoleProfile()
        {
            CreateMap<M_Sys_Role, D_Sys_Role>()
                .ForMember(d => d.id, m => m.MapFrom(x => x.roleId))
                .ForMember(d => d.role_name, m => m.MapFrom(x => x.roleName))
                .ForMember(d => d.description, m => m.MapFrom(x => x.description))
                .ForMember(d => d.sort, m => m.MapFrom(x => x.sort))
                .ForMember(d => d.add_user, m => m.MapFrom(x => x.addUser))
                .ForMember(d => d.add_time, m => m.MapFrom(x => x.addTime))
                .ForMember(d => d.update_user, m => m.MapFrom(x => x.updateUser))
                .ForMember(d => d.role_type, m => m.MapFrom(x => x.roleType))
                .ForMember(d => d.update_time, m => m.MapFrom(x => x.updateTime));

            CreateMap<D_Sys_Role, M_Sys_Role>()
                .ForMember(m => m.roleId, d => d.MapFrom(x => x.id))
                .ForMember(m => m.roleName, d => d.MapFrom(x => x.role_name))
                .ForMember(m => m.description, d => d.MapFrom(x => x.description))
                .ForMember(m => m.sort, d => d.MapFrom(x => x.sort))
                .ForMember(m => m.addUser, d => d.MapFrom(x => x.add_user))
                .ForMember(m => m.addTime, d => d.MapFrom(x => x.add_time))
                .ForMember(m => m.updateUser, d => d.MapFrom(x => x.update_user))
                .ForMember(d => d.roleType, m => m.MapFrom(x => x.role_type))
                .ForMember(m => m.updateTime, d => d.MapFrom(x => x.update_time));
        }
    }
}
