﻿using AutoMapper;
using Kjs.Report.Config;
using Kjs.Report.Admin.Handler;
using Kjs.Report.Infrastructure;
using Kjs.Report.JWT;
using Kjs.Report.Model.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using NLog.Extensions.Logging;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Kjs.Report.EntityFramework;

namespace Kjs.Report.Admin
{
    /// <summary>
    /// 应用程序的入口
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 配置根节点类接口
        /// </summary>
        public IConfigurationRoot ConfigurationRoot { get; set; }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()  //创建ConfigurationBuilder，其作用就是加载Congfig等配置文件 
            .SetBasePath(env.ContentRootPath)  //env.ContentRootPath：获取当前项目的跟路径 
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)  //使用AddJsonFile方法把项目中的appsettings.json配置文件加载进来，后面的reloadOnChange顾名思义就是文件如果改动就重新加载 
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true) //关注的是$"{param}"的这种写法，有点类似于string.Format() 
            .AddEnvironmentVariables();

            ConfigurationRoot = builder.Build(); //返回一个配置文件跟节点：IConfigurationRoot 
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //添加Mysql支持
            services.AddDbContext<EFCoreContext>(options => options.UseMySql(ConfigurationRoot.GetConnectionString("MySqlConnection")));
            SqlConfig.ConnectionString = ConfigurationRoot.GetConnectionString("MySqlConnection");

            //添加redis支持
            RedisHelper.InitializeConfiguration(ConfigurationRoot);

            //服务注册
            RegisterService("Kjs.Report.IService", services);
            RegisterService("Kjs.Report.Service", services);

            //第三方接口url注册
            //services.Configure<ApiUrls>(ConfigurationRoot.GetSection("ApiUrls"));

            ////添加UnitOfWork支持
            services.AddUnitOfWork<EFCoreContext>();

            #region jwt认证配置

            //添加跨域支持
            services.AddCors(options =>
            options.AddPolicy("Any", builder => builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin().AllowCredentials()));

            //读取配置文件
            var audienceConfig = ConfigurationRoot.GetSection("Audience");
            var symmetricKeyAsBase64 = audienceConfig["Secret"];
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,
                ValidateIssuer = true,
                ValidIssuer = audienceConfig["Issuer"],//发行人
                ValidateAudience = true,
                ValidAudience = audienceConfig["Audience"],//订阅人
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,

            };
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            //string[] SystemModuleArry = { "log", "menu", "role", "user"}, PlatformModuleArry = { "advertCarousel", "adverts", "articlecategory", "article", "articlegrouprule", "articlespec", "articlesupplier", "oauth", "orders", "payment" };

            //var permissions = new List<JwtPermission> {
            //    new JwtPermission(){ RoleName="System",ModuleArryList=SystemModuleArry }, //系统管理员
            //    new JwtPermission(){ RoleName="Operator",ModuleArryList=PlatformModuleArry}, //系统操作员
            //};
            var jwtRequirement = new JwtRequirement(
                //permissions,
                "/api/user/denied",
                ClaimTypes.Role,
                audienceConfig["Issuer"],
                audienceConfig["Audience"],
                signingCredentials,
                expiration: TimeSpan.FromDays(3650)//设置Token过期时间
                );


            services.AddAuthorization(options =>
            {
                options.AddPolicy("Bearer", policy => policy.Requirements.Add(jwtRequirement));

            })
            .AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = tokenValidationParameters;
                o.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        if (context.Request.Path.Value.ToString() == "/api/user/logout")
                        {
                            var token = ((context as TokenValidatedContext).SecurityToken as JwtSecurityToken).RawData;
                        }
                        return Task.CompletedTask;
                    }
                };
            });
            //注入授权Handler
            services.AddSingleton<IAuthorizationHandler, JwtAuthorizationHandler>();
            services.AddSingleton(jwtRequirement);

            #endregion

            services.AddAutoMapper();

            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ApiResultHandlerMiddleware));
                options.RespectBrowserAcceptHeader = true;
            })
            .AddJsonOptions(options => {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            });

            #region Swagger配置
            services.ConfigureSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "XXX报表API", Version = "v1", Description = "RESTful API for My Web Application", TermsOfService = "None" });
                //注意：此处替换成所生成的XML documentation的文件名
                options.IncludeXmlComments(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Kjs.Report.Admin.xml"));
                options.DescribeAllEnumsAsStrings();
                //手动高亮
                options.OperationFilter<AuthTokenHeaderFilter>();
                options.OperationFilter<UploadFilter>();
            });
            services.AddSwaggerGen();

            #endregion
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // 添加日志支持
            //loggerFactory.AddConsole(ConfigurationRoot.GetSection("Logging"));
            loggerFactory.AddDebug();

            //添加NLog
            loggerFactory.AddNLog();
            //读取Nlog配置文件 
            env.ConfigureNLog("nlog.config");

            app.UseCors("Any");

            if (env.IsEnvironment("develop"))
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c=> {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Admin API V1");
                });
            }

            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "ExcelExportFile")),
            //    RequestPath = "/ExcelExportFile"
            //});

            app.UseErrorHandling();

            app.UseMvc();
        }

        /// <summary>
        /// 集中服务注册
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="services"></param>
        private void RegisterService(string assemblyName, IServiceCollection services)
        {
            foreach (var item in GetClassName(assemblyName))
            {
                foreach (var typeArray in item.Value)
                {
                    services.AddScoped(typeArray, item.Key); //用ASP.NET Core自带依赖注入(DI)注入使用的类
                }
            }
        }

        /// <summary>  
		/// 获取程序集中的实现类对应的多个接口
		/// </summary>  
		/// <param name="assemblyName">程序集</param>
		private Dictionary<Type, Type[]> GetClassName(string assemblyName)
        {
            if (!string.IsNullOrEmpty(assemblyName))
            {
                Assembly assembly = Assembly.Load(assemblyName);
                List<Type> ts = assembly.GetTypes().ToList();

                var result = new Dictionary<Type, Type[]>();
                foreach (var item in ts.Where(s => !s.IsInterface))
                {
                    var interfaceType = item.GetInterfaces();
                    result.Add(item, interfaceType);
                }
                return result;
            }
            return new Dictionary<Type, Type[]>();
        }
    }
}
