﻿using Kjs.Report.Model.Extensions;
using System;

namespace Kjs.Report.Business
{
    public class ImageExtension
    {
        private readonly ApiUrls _apiUrls;
        public ImageExtension(ApiUrls apiUrls)
        {
            _apiUrls = apiUrls;
        }

        /// <summary>
        /// 获取图片完整路径
        /// </summary>
        /// <param name="relPath"></param>
        /// <returns></returns>
        public string GetImgAbsolutePath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                return (path.Contains("http") || path.Contains("https")) ? path : _apiUrls.AliyunOss + path;
            }
            return "";
        }

        /// <summary>
        /// 获取图片相对路径
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <returns></returns>
        public string GetImgRelativePath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                return new Uri(path).AbsolutePath;
            }
            return "";
        }
    }
}
