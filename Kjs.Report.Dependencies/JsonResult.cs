﻿namespace Luxtek.Dependencies
{
    public class JsonResult<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }
}
