﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kjs.Report.Domain.Table
{
    [Table("sys_user")]
    public class D_Sys_User : BaseEntity<int>
    {
        public string head_img { get; set; }
        public string user_no { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string real_name { get; set; }
        public int? role_id { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public bool? is_super { get; set; }
        public bool? is_enable { get; set; }
        public int? add_user { get; set; }
        public DateTime? add_time { get; set; }
        public int? update_user { get; set; }
        public DateTime? update_time { get; set; }
        public string last_login_ip { get; set; }
        public DateTime? last_login_time { get; set; }
    }
}
