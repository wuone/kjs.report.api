﻿using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.IService
{
    /// <summary>
    /// 促销商品
    /// </summary>
    public interface IPromotionGoodService
    {
        /// <summary>
        /// 按照商品名称分组统计销售数据明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_PromotionGoodTitleGroup>> GoodNameGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);


        /// <summary>
        /// 按照时间分组统计销售数据明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_PromotionTimeGroup>> TimeGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);

        /// <summary>
        /// 按照时间及商品名称分组统计销售数据明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_PromotionGoodTitleAndTimeGroup>> TitleAndTimeGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);
    }
}
