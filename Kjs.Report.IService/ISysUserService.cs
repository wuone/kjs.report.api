﻿using Kjs.Report.Domain.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Kjs.Report.IService
{
    public interface ISysUserService
    {
        Task<D_Sys_User> Get(string userName);

        Task<List<D_Sys_User>> GetUserList();

        Task<int> Add(D_Sys_User user);

        Task<int> Update(D_Sys_User user);

        int Delete(int userId);

        int UpdateUserPwd(int userId, string newPwd);

        int UpdateUserRole(int roleId, int userId);

        bool CheckUserPwd(int userId, string oldPwd);

        Task<List<int>> GetUserIdsByRoleId(int roleId);

        bool ExistsNameOrNo(string userName, string userNo, int userId);
    }
}
