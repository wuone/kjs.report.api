﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model.Enums
{
    public enum ELogType
    {
        /// <summary>
        /// 系统登录
        /// </summary>
        SysLogin,

        /// <summary>
        /// 添加用户
        /// </summary>
        AddUser,
        /// <summary>
        /// 编辑用户
        /// </summary>
        EditUser,
        /// <summary>
        /// 删除用户
        /// </summary>
        DeleteUser,

        /// <summary>
        /// 修改用户角色
        /// </summary>
        EditUserRole,

        /// <summary>
        /// 修改用户密码
        /// </summary>
        EditUserPwd,

        /// <summary>
        /// 添加菜单
        /// </summary>
        AddMenu,
        /// <summary>
        /// 编辑菜单
        /// </summary>
        EditMenu,
        /// <summary>
        /// 删除菜单
        /// </summary>
        DeleteMenu,

        /// <summary>
        /// 添加角色
        /// </summary>
        AddRole,
        /// <summary>
        /// 编辑角色
        /// </summary>
        EditRole,
        /// <summary>
        /// 删除角色
        /// </summary>
        DeleteRole,

        /// <summary>
        /// 添加角色菜单
        /// </summary>
        AddRoleMenu,
        /// <summary>
        /// 添加规格
        /// </summary>
        AddSpec,
        /// <summary>
        /// 修改规格
        /// </summary>
        UpdateSpec,
        /// <summary>
        /// 删除规格
        /// </summary>
        DeleteSpec,
        /// <summary>
        /// 设置商品规格
        /// </summary>
        SetGoodSpec,
        /// <summary>
        /// 添加分类
        /// </summary>
        AddCategory,
        /// <summary>
        /// 修改分类
        /// </summary>
        UpdateCategory,
        /// <summary>
        /// 删除分类
        /// </summary>
        DeleteCategory,
        /// <summary>
        /// 添加商品
        /// </summary>
        AddGoods,
        /// <summary>
        /// 修改商品
        /// </summary>
        UpdateGoods,
        /// <summary>
        /// 添加广告
        /// </summary>
        AddAdvert,
        /// <summary>
        /// 修改广告
        /// </summary>
        UpdateAdvert,
        /// <summary>
        /// 删除广告
        /// </summary>
        DeleteAdvert,
        /// <summary>
        /// 添加广告轮播
        /// </summary>
        AddAdvertCarouse,
        /// <summary>
        /// 修改广告轮播
        /// </summary>
        UpdateAdvertCarouse,
        /// <summary>
        /// 删除广告轮播
        /// </summary>
        DeleteAdvertCarouse,
        /// <summary>
        /// 添加oauth
        /// </summary>
        AddOauth,
        /// <summary>
        /// 修改oauth
        /// </summary>
        UpdateOauth,
        /// <summary>
        /// 添加支付配置
        /// </summary>
        AddPayment,
        /// <summary>
        /// 修改支付配置
        /// </summary>
        UpdatePayment,
        /// <summary>
        /// 添加供应商
        /// </summary>
        AddSupplier,
        /// <summary>
        /// 修改供应商
        /// </summary>
        UpdateSupplier
    }
}
