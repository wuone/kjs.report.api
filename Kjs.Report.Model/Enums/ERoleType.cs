﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model.Enums
{
    public enum ERoleType
    {
        /// <summary>
        /// 超级管理员
        /// </summary>
        Admin,
        /// <summary>
        /// 平台管理员
        /// </summary>
        platform,
        /// <summary>
        /// 系统管理员
        /// </summary>
        System,
    }
}
