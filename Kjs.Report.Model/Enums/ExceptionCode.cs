﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model.Enums
{
    public enum ExceptionCode
    {
        /// <summary>
        /// 业务数据错误
        /// </summary>
        DataError = 2000,

        /// <summary>
        /// 业务数据不存在
        /// </summary>
        DataNotFound = 2002,

        /// <summary>
        /// 数据格式错误
        /// </summary>
        DataFormatError = 2003,

        /// <summary>
        /// 数据类型错误
        /// </summary>
        DataTypeError = 2004,

        /// <summary>
        /// 数据重复
        /// </summary>
        DataRepeat = 2005,

        /// <summary>
        /// 数据没有授权
        /// </summary>
        DataUnsupported = 2006,

        /// <summary>
        /// 数据无效
        /// </summary>
        DataInvalid = 2007,

        /// <summary>
        /// 参数错误
        /// </summary>
        ParameterError = 3000
    }
}
