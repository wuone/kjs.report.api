﻿using System;
using System.Security.Claims;

namespace Kjs.Report.Model.Extensions
{
    public static class IPrincipalExtensions
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string userId(this ClaimsPrincipal user) => Convert.ToString(user.FindFirst("userId").Value);

        ///// <summary>
        ///// 用户编号
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static string userNo(this ClaimsPrincipal user) => user.FindFirst("userNo").Value;

        ///// <summary>
        ///// 用户名
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static string userName(this ClaimsPrincipal user) => user.FindFirst("userName").Value;

        ///// <summary>
        ///// 真实姓名
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static string realName(this ClaimsPrincipal user) => user.FindFirst("realName").Value;

        ///// <summary>
        ///// 角色ID
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static int roleId(this ClaimsPrincipal user) => Convert.ToInt32(user.FindFirst("roleId").Value);

        ///// <summary>
        ///// email
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static string email(this ClaimsPrincipal user) => user.FindFirst("email").Value;

        ///// <summary>
        ///// 手机号码
        ///// </summary>
        ///// <param name="user"></param>
        ///// <returns></returns>
        //public static string phone(this ClaimsPrincipal user) => user.FindFirst("phone").Value;
    }
}
