﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    public class M_GoodSaleGroup
    {
        /// <summary>
        /// 商品名称
        /// </summary>
        public string good_name { get; set; }
        /// <summary>
        /// 销售价
        /// </summary>
        public decimal sell_price { get; set; }
        /// <summary>
        /// 库存
        /// </summary>
        public int stock_quantity { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal total_money { get; set; }
    }
}
