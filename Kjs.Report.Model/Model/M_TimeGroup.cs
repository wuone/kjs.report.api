﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.Model
{
    public class M_TimeGroup
    {
        /// <summary>
        /// 日期
        /// </summary>
        public string time { get; set; }
        /// <summary>
        /// 总销量
        /// </summary>
        public int total_count { get; set; }

        /// <summary>
        /// 总销售额
        /// </summary>
        public decimal total_amount { get; set; }
    }
}
